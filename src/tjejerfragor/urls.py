"""tjejerfragor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from api.urls import router as base_routes
from user.urls import router as user_routes
from likes.urls import router as like_routes
from follow.urls import router as follower_routes
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers
from rest_framework.response import Response
from ratelimit.exceptions import Ratelimited

urls = routers.DefaultRouter()
urls.registry.extend(base_routes.registry)
urls.registry.extend(user_routes.registry)
urls.registry.extend(like_routes.registry)
urls.registry.extend(follower_routes.registry)

urlpatterns = [
    path('', include(urls.urls), name=''),
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG == 'True':
    import debug_toolbar
    urlpatterns += path('__debug__/', include(debug_toolbar.urls)),
    urlpatterns += path('admin/clearcache/', include('clearcache.urls')),


def handler403(request, exception=None):
    if isinstance(exception, Ratelimited):
        return Response(status=429, data={"message": 'noluyor amk siktin sistemi'})
    # return HttpResponseForbidden('Forbidden')
