from django.db import models
from django.conf import settings
from .choices import GENDER_CHOICES, RELATION_CHOICES
from helpers.validators import validate_file_extension
from django.contrib.auth.models import User
# User = settings.AUTH_USER_MODEL


class UserDetail(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    about_me = models.TextField(null=True, blank=True)
    relationship = models.CharField(
        max_length=100, null=True, blank=True, choices=RELATION_CHOICES)
    career = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    profile_picture = models.ImageField(upload_to='users/', blank=True,
                                        validators=[validate_file_extension])
    main_picture = models.ImageField(upload_to='users/', blank=True, default="main_picture.png",
                                     validators=[validate_file_extension])
    gender = models.CharField(
        max_length=10, default=GENDER_CHOICES, choices=GENDER_CHOICES)
    user_level = models.IntegerField(default=1, blank=True)
    sponsored = models.BooleanField(default=False, blank=True)
    hidden_profile = models.BooleanField(default=False)
    first_steps = models.SmallIntegerField(default=1, null=False, blank=True)
    disabled = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        if self.gender == 'man':
            self.profile_picture = 'no-male.png'
        elif self.gender == 'woman':
            self.profile_picture = 'no-female.png'


class UserLike(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)
    question = models.ForeignKey(
        'api.Question', on_delete=models.CASCADE, null=True, blank=True)
    post = models.ForeignKey(
        'api.Post', on_delete=models.CASCADE, null=True, blank=True)
    comment = models.ForeignKey(
        'api.Comment', on_delete=models.CASCADE, null=True, blank=True)
    liked = models.BooleanField(default=False)
    dissliked = models.BooleanField(default=False)

    def __str__(self):
        if self.post:
            return self.post.title
        if self.question:
            return self.question.title
        if self.comment:
            return self.comment.content

        return str(self.id)
