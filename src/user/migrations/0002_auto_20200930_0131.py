# Generated by Django 3.1.1 on 2020-09-30 01:31

from django.db import migrations, models
import helpers.validators


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdetail',
            name='main_picture',
            field=models.ImageField(blank=True, default='main_picture.png', upload_to='users/', validators=[helpers.validators.validate_file_extension]),
        ),
    ]
