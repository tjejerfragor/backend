from rest_framework import routers
from .views import LoginViewset, RegisterViewset, UserAccount

router = routers.SimpleRouter()
router.register('login', LoginViewset, 'Login')
router.register('register', RegisterViewset, 'Register')
router.register('user', UserAccount, 'UserAccount')
