GENDER_CHOICES = (
    ('woman', "Tjej"),
    ('man', "Kille"),
    ('lgbt', "LGBT")
)

RELATION_CHOICES = (
    ('single', "Singel"),
    ('relation', "Förhållande"),
    ('married', "Gift"),
    ('divorced', "Skild")
)