from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import UserDetail
from .serializers import UserSerializer, LoginSerializer, RegisterSerializer, UserDetailSerializer


class UserAccount(viewsets.ViewSet):
    http_method_names = ['get']
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def list(self, request):
        queryset = User.objects.filter(username=request.user)
        serializer = UserSerializer(queryset, many=True,context={'request': request})
        user_detail_serializer = UserDetailSerializer(UserDetail.objects.filter(user=request.user), many=True,context={'request': request})

        return Response({"user": {**serializer.data[0], **user_detail_serializer.data[0]}})


class LoginViewset(viewsets.ViewSet):
    http_method_names = ['post']

    def create(self, request):
        from rest_framework.authtoken.models import Token

        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, created = Token.objects.get_or_create(user=user)

        return Response({"token": token.key}, status=200)


class RegisterViewset(viewsets.ModelViewSet):
    http_method_names = ['post']
    serializer_class = RegisterSerializer
