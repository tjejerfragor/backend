from rest_framework import serializers
from .models import UserDetail
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
import api.serializers as ApiSerializers
import api.models as ApiModels


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email')
        read_only_fields = fields


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)

    def validate(self, data):
        email = data.get("email")
        password = data.get("password")

        user = authenticate(username=email, password=password)
        if not user:
            msg = "Det gick inte att logga in med angivna uppgifter inloggningsuppgifter."
            raise ValidationError(msg)

        data["user"] = user
        return data


class RegisterSerializer(serializers.ModelSerializer):
    password_confirmation = serializers.CharField(write_only=True)
    email = serializers.EmailField(required=True)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    gender = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'password_confirmation',
            'email',
            'first_name',
            'last_name',
            'gender'
        ]

        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }

    def validate(self, validate_data):
        if validate_data['password_confirmation'] == validate_data['password']:
            return validate_data

        raise ValidationError('Lösenorded matchar inte med varandra.')

    def create(self, validated_data):
        username = validated_data.get('username')
        email = validated_data.get('email')
        password = validated_data.get('password')

        user_obj = User(username=username, email=email)
        user_obj.set_password(password)
        user_obj.save()

        user_detail = UserDetail(user=user_obj)
        user_detail.gender = validated_data.get('gender')
        user_detail.save()

        return validated_data


class UserDetailSerializer(serializers.ModelSerializer):
    user_data = UserSerializer(source='user', required=False)
    questions = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()

    def get_questions(self, user: UserDetail):
        if not 'additional_fields' in self.context:
            return None
        queryset = ApiModels.Question.objects.filter(
            user=user.user, hidden_author=False).select_related('user', 'user_details', 'category')

        serializer = ApiSerializers.QuestionSerializer(queryset, many=True)

        return serializer.data

    def get_comments(self, user: UserDetail):
        if not 'additional_fields' in self.context:
            return None
        queryset = ApiModels.Comment.objects.filter(
            user=user.user,  hidden_author=False).select_related('user', 'user_details', 'post', 'question', 'parent')

        serializer = ApiSerializers.CommentSerializer(queryset, many=True)

        return serializer.data

    def setup_eager_loading(self, queryset):
        # select_related for 'to-one' relationships
        queryset = queryset.select_related(
            'user')
        return queryset

    class Meta:
        model = UserDetail
        fields = (
            "id",
            "about_me",
            "relationship",
            "career",
            "city",
            "profile_picture",
            "main_picture",
            "gender",
            "user_level",
            "sponsored",
            "hidden_profile",
            "user",
            "user_data",
            "questions",
            "comments",
        )
