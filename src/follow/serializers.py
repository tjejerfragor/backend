from rest_framework import serializers
from .models import Follower
from user.serializers import UserSerializer, UserDetailSerializer

class MyFollowersSerializer(serializers.ModelSerializer):
    actor_details = UserDetailSerializer(source='actor', required=False)
    user_details = UserDetailSerializer(source='user', required=False)

    class Meta:
        model = Follower
        fields = '__all__'