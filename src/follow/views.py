from rest_framework.viewsets import ModelViewSet
from .models import Follower
from .serializers import MyFollowersSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from user.models import UserDetail
from helpers.pagination import StandardResultsSetPagination


class Followers(ModelViewSet):
    serializer_class = MyFollowersSerializer
    http_method_names = ['get', 'post', 'delete']
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        users_following_me = self.request.query_params.get('following')

        if users_following_me and users_following_me == 'me':
            return Follower.objects.filter(actor__user=self.request.auth.user)

        return Follower.objects.filter(user__user=self.request.user)

    def create(self, request):
        user_id = self.request.user.id
        actor_id = self.request.POST.get('actor')

        if not self.check_if_user_follows_user(user_id, actor_id):
            add_user = Follower()
            add_user.user = UserDetail.objects.get(user=self.request.user)
            add_user.actor = UserDetail.objects.get(user=actor_id)
            add_user.save()

            return Response({
                'status': True,
                'status_code': 'followed'
            })

        return Response({
            'status': False,
            'status_code': 'already_following'
        })

    def destroy(self, request, *args, **kwargs):
        user_id = self.request.user.id
        actor_id = kwargs.get('pk')

        if self.check_if_user_follows_user(user_id, actor_id):
            print(user_id, actor_id)
            Follower.objects.get(user__user=user_id, actor__user=actor_id).delete()

            return Response({
                "status": True,
                'status_code': 'unfollowed'
            })
        else:
            return Response({
                'status': False,
                'status_code': 'not_following'
            })

    def check_if_user_follows_user(self, user_id, leader_id):
        query = Follower.objects.filter(user__user=user_id, actor__user=leader_id).exists()

        if query:
            return True

        return False
