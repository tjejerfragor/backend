from rest_framework import routers
from .views import Followers

router = routers.SimpleRouter()
router.register('followers', Followers, 'Followers')
