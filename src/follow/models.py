from django.db import models
from django.contrib.auth.models import User


class Follower(models.Model):
    user = models.ForeignKey('user.UserDetail', on_delete=models.CASCADE, related_name='follow_executer_user')
    actor = models.ForeignKey('user.UserDetail', on_delete=models.CASCADE, related_name='follow_actor_user')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)