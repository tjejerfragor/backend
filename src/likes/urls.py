from rest_framework import routers
from .views import PostLikeViewset

router = routers.SimpleRouter()
router.register('likes', PostLikeViewset, 'Likes')