from rest_framework import viewsets, filters
from .models import PostLike
from .serializers import PostLikeSerializer
from rest_framework.authentication import TokenAuthentication
from helpers.middleware import PostMethodAuthRequired
from django_filters.rest_framework import DjangoFilterBackend
from helpers.pagination import StandardResultsSetPagination
from rest_framework.response import Response

from api.models import Question, Comment, Post
from user.models import UserLike

class PostLikeViewset(viewsets.ModelViewSet):
    queryset = PostLike.objects.all()
    serializer_class = PostLikeSerializer
    http_method_names = ('get', 'put', 'delete')
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (PostMethodAuthRequired,)
    ordering_fields = ('question', 'post',)
    search_fields = ("post", "question", "comment",)
    filter_fields = ['question', 'post', 'comment']
    pagination_class = StandardResultsSetPagination

    def update(self, request, *args, **kwargs):
        data = {
            "user": request.user,
            "liked": True
        }

        if request.data.get('isQuestion'):
            data['question'] = Question.objects.get(id=request.data.get('isQuestion'))
            exists = UserLike.objects.filter(question=data['question']).exists()

        elif request.data.get('isComment'):
            data['comment'] = Comment.objects.get(id=request.data.get('isComment'))
            exists = UserLike.objects.filter(comment=data['comment']).exists()
        else:
            data['post'] = Post.objects.get(id=request.data.get('isPost'))
            exists = UserLike.objects.filter(post=data['post']).exists()

        if not exists:
            post_like = PostLike.objects.filter(id=kwargs['pk']).get()

            if request.data['gender'] == 'woman':
                post_like.girl_like_count = post_like.girl_like_count + 1
            else:
                post_like.boy_like_count = post_like.boy_like_count + 1

            user_likes = UserLike(**data)
            user_likes.save()
            post_like.save()

        else:
            return Response({"status": False})
        return Response({"status": True})

    def destroy(self, request, *args, **kwargs):
        data = {
            "user": request.user,
            "dissliked": True,
            'comment': Comment.objects.get(id=request.data.get('isComment'))
        }

        exists = UserLike.objects.filter(comment=data['comment'], dissliked=True).exists()

        if not exists:
            post_like = PostLike.objects.filter(id=kwargs['pk']).get()

            if request.data['gender'] == 'woman':
                post_like.girl_disslike_count = post_like.girl_disslike_count + 1
            else:
                post_like.boy_disslike_count = post_like.boy_disslike_count + 1

            user_likes = UserLike(**data)
            user_likes.save()
            post_like.save()

        else:
            return Response({"status": False})
        return Response({"status": True})