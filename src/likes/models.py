from django.db import models

class PostLike(models.Model):
    id = models.AutoField(primary_key=True)
    girl_like_count = models.IntegerField(default=0)
    boy_like_count = models.IntegerField(default=0)
    girl_disslike_count = models.IntegerField(default=0)
    boy_disslike_count = models.IntegerField(default=0)
    question = models.ForeignKey('api.Question', on_delete=models.CASCADE, null=True, blank=True)
    post = models.ForeignKey('api.Post', on_delete=models.CASCADE, null=True, blank=True)
    comment = models.ForeignKey('api.Comment', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        if self.post:
            return self.post.title
        if self.question:
            return self.question.title
        if self.comment:
            return self.comment.content

        return str(self.id)