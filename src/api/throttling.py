from rest_framework.throttling import UserRateThrottle

class LimitedFunctionalityThrottling(UserRateThrottle):
    scope = 'sustained'

    THROTTLE_RATES = {
        'sustained': '1/min'
    }