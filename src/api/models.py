import os
from tjejerfragor.settings import STATIC_ROOT
from django.db import models
from django.contrib.auth.models import User
from .choices import CONTENT_TYPE_CHOCIES, NOTIFICATION_TYPE_CHOICES
from .validators import validate_file_extension
from user.models import UserDetail
from likes.models import PostLike


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20, blank=False, null=False)
    description = models.TextField(default='', blank=False, null=False)
    slug = models.SlugField(max_length=20, blank=True)
    image = models.ImageField(upload_to='category', default='main_picture.png', validators=[
                              validate_file_extension])

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        from django.template.defaultfilters import slugify

        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)


class Post(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100, blank=False, null=False)
    content = models.TextField(blank=False, null=False)
    short_content = models.CharField(max_length=255, blank=True, null=True)
    slug = models.SlugField(max_length=150, blank=True)
    image = models.ImageField(upload_to='posts', default='noimage.png', null=False,
                              validators=[validate_file_extension])
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    user_details = models.ForeignKey(
        UserDetail, on_delete=models.CASCADE, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    disable_comments = models.BooleanField(default=False, null=True)
    disable_man = models.BooleanField(default=False)
    disable_woman = models.BooleanField(default=False)
    disable_anonymous = models.BooleanField(default=False)
    approved = models.BooleanField(default=False, null=True)
    editor_picked = models.BooleanField(default=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    admin_list_editable = (
        'disable_comments', 'disable_man', 'disable_woman', 'disable_anonymous', 'approved', 'editor_picked',)
    admin_list_display = (
        'title', 'slug', 'user', 'disable_comments', 'disable_man', 'disable_woman', 'disable_anonymous',
        'approved', 'editor_picked',)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        from django.template.defaultfilters import slugify
        from random import randint
        from .helpers import excerpt, clean_html, compress_icon

        random_int = randint(10000, 50000)
        slugify_title = f'{self.title[:60]} {random_int}'
        self.slug = slugify(slugify_title)
        self.short_content = clean_html(excerpt(self.content, 140))
        self.image = compress_icon(self.image)
        super(Post, self).save(*args, **kwargs)
        post_like = PostLike.objects.filter(post=self)

        if post_like.exists():
            post_like.update(post=self)
        else:
            post_like.create(post=self)


class Question(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100, blank=False, null=False)
    content = models.TextField(blank=True, null=True)
    short_content = models.CharField(max_length=255, blank=True, null=True)
    content_type = models.SmallIntegerField(
        default=0, choices=CONTENT_TYPE_CHOCIES)
    slug = models.SlugField(max_length=150, blank=True)
    image = models.ImageField(upload_to='questions', blank=True,
                              validators=[validate_file_extension], null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    user_details = models.ForeignKey(
        UserDetail, on_delete=models.CASCADE, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    disable_comments = models.BooleanField(default=False, null=True)
    disable_man = models.BooleanField(default=False)
    disable_woman = models.BooleanField(default=False)
    disable_anonymous = models.BooleanField(default=False)
    hidden_author = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    admin_list_editable = ('disable_comments', 'disable_man',
                           'disable_woman', 'disable_anonymous',)
    admin_list_display = (
        'title', 'slug', 'user', 'disable_comments', 'disable_man', 'disable_woman', 'disable_anonymous',)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        from django.template.defaultfilters import slugify
        from random import randint
        from .helpers import excerpt, clean_html, compress_icon

        random_int = randint(10000, 50000)
        slugify_title = f'{self.title[:60]} {random_int}'
        self.slug = slugify(slugify_title)
        self.short_content = clean_html(excerpt(self.content, 140))
        self.image = compress_icon(self.image)
        super(Question, self).save(*args, **kwargs)
        post_like = PostLike.objects.filter(question=self)

        if post_like.exists():
            post_like.update(question=self)
        else:
            post_like.create(question=self)


class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    user_details = models.ForeignKey(
        UserDetail, on_delete=models.CASCADE, null=True, blank=True)
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, blank=True, null=True)
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, blank=True, null=True)
    parent = models.ForeignKey(
        'self', on_delete=models.CASCADE, blank=True, null=True)
    hidden_author = models.BooleanField(default=False)
    content = models.TextField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        super(Comment, self).save(*args, **kwargs)

        m = PostLike()
        m.comment = self
        m.save()

    def __str__(self):
        return f'{self.content} | {self.user.username}'

#
# class Badge(models.Model):
#     id = models.AutoField(primary_key=True)
#     badge_name = models.CharField(max_length=20)
#     badge_earn_level = models.SmallIntegerField()
#
#     def __str__(self):
#         return self.badge_name


class NotificationManager(models.Manager):
    def get_post(self, target_object_id):
        target = None
        post_notifications = Notification.objects.filter(
            target_type='post', target_object_id=target_object_id)

        if Post.objects.filter(id=post_notifications[0].target_object_id).exists():
            target = Post.objects.filter(id=target_object_id)

        return {
            'target': target
        }

    def get_question(self, target_object_id):
        target = None
        question_notifications = Notification.objects.filter(
            target_type='question', target_object_id=target_object_id)

        if Question.objects.filter(id=question_notifications[0].target_object_id).exists():
            target = Question.objects.filter(id=target_object_id)

        return {
            'target': target
        }

    def get_comment(self, target_object_id):
        target = None
        comment_notifications = Notification.objects.filter(
            target_type='comment', target_object_id=target_object_id)

        if Comment.objects.filter(id=comment_notifications[0].target_object_id).exists():
            target = Comment.objects.filter(id=target_object_id)

        return {
            'target': target
        }


class Notification(models.Model):
    id = models.AutoField(primary_key=True)
    executer = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='executer_user')
    actor = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='actor_user')
    target_type = models.CharField(
        max_length=25, choices=NOTIFICATION_TYPE_CHOICES, null=False)
    target_object_id = models.SmallIntegerField()
    verb = models.CharField(max_length=35, null=False, blank=True)
    readed = models.BooleanField(default=False)
    objects = NotificationManager()
    admin_list_editable = ('readed',)

    def __str__(self):
        target = None
        if self.target_type == 'post':
            if Post.objects.filter(id=self.target_object_id).exists():
                target = Post.objects.get(id=self.target_object_id)

        if self.target_type == 'question':
            if Question.objects.filter(id=self.target_object_id).exists():
                target = Question.objects.get(id=self.target_object_id)

        if self.target_type == 'comment':
            if Comment.objects.filter(id=self.target_object_id).exists():
                target = Comment.objects.get(id=self.target_object_id)

        return f'{self.executer.username} {self.verb} på {target.title if hasattr(target, "title") else target.content}'
