from rest_framework import serializers
from .models import Post, Question, Comment, Category, Notification
from django.core.exceptions import ValidationError
from likes.serializers import PostLikeSerializer
from likes.models import PostLike
import user.serializers as UserSerializers
import likes.serializers as LikesSerializers


class NotificationSerializer(serializers.ModelSerializer):
    target_type = serializers.CharField(required=False)
    target_object_id = serializers.CharField(required=False)

    class Meta:
        model = Notification
        fields = ('id', 'actor', 'executer', 'readed',
                  'target_type', 'target_object_id', 'verb',)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['name', 'description', 'slug', 'image']
        read_only_fields = fields


class PostSerializer(serializers.ModelSerializer):
    category_data = CategorySerializer(source='category', required=False)
    image = serializers.ImageField(required=True)
    user_details_data = UserSerializers.UserDetailSerializer(
        source='user_details', required=False, read_only=True)
    user_data = UserSerializers.UserSerializer(source='user', required=False)
    likes_data = serializers.SerializerMethodField()

    def get_likes_data(self, post: Post):
        query = PostLike.objects.get(post=post.id)

        return PostLikeSerializer(query).data

    def validate(self, validated_data):
        content = dict(validated_data)['content']
        title = dict(validated_data)['title']

        error_fields = {}

        if len(title) < 20:
            error_fields['title'] = 'Titeln måste vara mer än 20 tecken.'

        if len(title) > 150:
            error_fields['title'] = 'Titeln kan inte vara mer än 150 tecken.'

        if len(content) < 250:
            error_fields['content'] = 'Innehåll måste vara mer än 250 tecken.'

        if len(error_fields) > 0:
            raise ValidationError(error_fields)

        return validated_data

    def setup_eager_loading(self, queryset):
        # select_related for 'to-one' relationships
        queryset = queryset.select_related('user', 'category', 'user_details')
        return queryset

    class Meta:
        model = Post
        fields = ('id', 'title', 'content', 'slug', 'image', 'disable_comments', 'disable_man', 'disable_woman',
                  'disable_anonymous', 'created_at', 'updated_at', 'user', 'category',
                  'user_details', 'user_data', 'user_details_data',
                  'short_content', 'category_data', 'editor_picked', 'likes_data',)

        read_only_fields = [
            'id', 'slug',  'disable_comments', 'disable_man', 'disable_woman',
            'disable_anonymous', 'created_at', 'updated_at', 'user',
                  'user_details', 'user_data', 'user_details_data',
                  'short_content', 'category_data', 'editor_picked', 'likes_data',
        ]


class QuestionSerializer(serializers.ModelSerializer):
    category_data = CategorySerializer(source='category', required=False)
    user_details_data = serializers.SerializerMethodField(
        source='user_details', required=False)
    user_data = serializers.SerializerMethodField(
        source='user', required=False)
    likes_data = serializers.SerializerMethodField()

    def get_likes_data(self, obj):
        query = PostLike.objects.get(question=obj.id)

        return PostLikeSerializer(query).data

    def get_user_data(self, obj):
        if obj.hidden_author == True:
            return {
                "id": obj.user.id
            }

        return UserSerializers.UserSerializer(obj.user).data

    def get_user_details_data(self, obj):
        if obj.hidden_author == True:
            return {
                "gender": obj.user_details.gender
            }

        return UserSerializers.UserDetailSerializer(obj.user_details).data

    def validate(self, validated_data):
        title = dict(validated_data)['title']

        error_fields = {}

        if len(title) < 15:
            error_fields['title'] = 'Titeln måste vara mer än 20 tecken.'

        if len(title) > 150:
            error_fields['title'] = 'Titeln kan inte vara mer än 150 tecken.'

        if len(error_fields) > 0:
            raise ValidationError(error_fields)

        return validated_data

    def setup_eager_loading(self, queryset):
        # select_related for 'to-one' relationships
        queryset = queryset.select_related('user', 'category', 'user_details')
        return queryset

    class Meta:
        model = Question
        fields = ('id', 'title', 'content', 'slug', 'image', 'disable_comments', 'disable_man', 'disable_woman',
                  'disable_anonymous', 'created_at', 'updated_at', 'user', 'category', 'user_details', 'user_data',
                  'user_details_data',
                  'short_content', 'category_data', 'content_type', 'hidden_author', 'likes_data',)

        read_only_fields = [
            'id', 'slug',  'disable_comments', 'disable_man', 'disable_woman',
                  'disable_anonymous', 'created_at', 'updated_at', 'user', 'user_details', 'user_data',
                  'user_details_data',
                  'short_content', 'category_data', 'content_type', 'hidden_author', 'likes_data'
        ]


class CommentSerializer(serializers.ModelSerializer):
    post_data = PostSerializer(source='post', required=False)
    question_data = QuestionSerializer(source='question', required=False)
    likes_data = serializers.SerializerMethodField(source='id', required=False)
    user_details_data = serializers.SerializerMethodField(
        source='user_details', required=False)
    user_data = serializers.SerializerMethodField(
        source='user', required=False)

    def get_user_data(self, obj):
        if obj.hidden_author == True:
            print("is true")
            return {
                "id": obj.user.id
            }

        return UserSerializers.UserSerializer(obj.user).data

    def get_user_details_data(self, obj):
        if obj.hidden_author == True:
            return {
                "gender": obj.user_details.gender
            }

        return UserSerializers.UserDetailSerializer(obj.user_details).data

    def get_likes_data(self, obj):
        query = PostLike.objects.get(comment=obj.id)
        return PostLikeSerializer(query).data

    def setup_eager_loading(self, queryset):
        """ Perform necessary eager loading of data. """
        queryset = queryset.prefetch_related(
            'user', 'user_details', 'post', 'question', 'parent')
        return queryset

    class Meta:
        model = Comment
        fields = (
            'id', 'post', 'user', 'question', 'parent', 'hidden_author', 'content', 'created_at', 'user_details',
            'user',
            'user_data',
            'user_details_data', 'post_data', 'question_data', 'likes_data',)

        read_only_fields = [
            'id', 'user',  'created_at', 'user_details',
            'user',
            'user_data',
            'user_details_data', 'post_data', 'question_data', 'likes_data'
        ]
