# Generated by Django 3.1.1 on 2020-09-30 01:31

import api.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20200830_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='image',
            field=models.ImageField(default='main_picture.png', upload_to='category', validators=[api.validators.validate_file_extension]),
        ),
        migrations.AlterField(
            model_name='question',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='questions', validators=[api.validators.validate_file_extension]),
        ),
    ]
