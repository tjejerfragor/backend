from rest_framework import routers
from api.views import PostViewset, QuestionViewset, UserViewset, CommentViewset, CategoryViewset, \
    UserDetailViewset, PostAndQuestionViewset, NotificationViewset, \
    ImageUploader

router = routers.SimpleRouter()
router.register('posts', PostViewset, 'Posts')
router.register('all', PostAndQuestionViewset, 'PostsAndQuestions')
router.register('questions', QuestionViewset, 'Questions')
router.register('users', UserViewset, 'Users')
router.register('userdetail', UserDetailViewset, 'UserDetail')
router.register('comments', CommentViewset, 'Comments')
router.register('categories', CategoryViewset, 'Category')
router.register('notifications', NotificationViewset, 'Notification')
router.register('upload', ImageUploader, 'ImageUploader')
