from rest_framework import viewsets, filters, status
from .models import Post, Question, Comment, Category, Notification
from .serializers import PostSerializer, QuestionSerializer, CommentSerializer, CategorySerializer, NotificationSerializer
from rest_framework.authentication import TokenAuthentication
from helpers.middleware import PostMethodAuthRequired
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth.models import User
from helpers.pagination import StandardResultsSetPagination
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from ratelimit.decorators import ratelimit
from user.models import UserDetail
from user.serializers import UserDetailSerializer, UserSerializer
from typing import Union
from .constants import GENDER_MALE, GENDER_FEMALE
from .error_codes import comment_disabled_for_your_gender, comment_disabled_for_your_anonymous

from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie
from django.utils.decorators import method_decorator
from rest_framework.exceptions import NotAcceptable


class PostViewset(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (PostMethodAuthRequired,)
    http_method_names = ('get', 'post')
    filter_backends = (filters.SearchFilter,
                       filters.OrderingFilter, DjangoFilterBackend,)
    filter_fields = ['category', 'approved', 'category__name',
                     'category__slug', 'user_details__gender']
    ordering_fields = ('created_at', 'updated_at',)
    search_fields = ("title", 'content', 'slug')
    pagination_class = StandardResultsSetPagination

    # Cache page for 1 day

    @method_decorator(cache_page(86400))
    @method_decorator(vary_on_cookie)
    def dispatch(self, *args, **kwargs):
        return super(PostViewset, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        qs = Post.objects.filter(approved=True)

        return self.serializer_class().setup_eager_loading(qs)

    def perform_create(self, serializer: PostSerializer):
        user = self.request.user
        user_details = UserDetail.objects.get(user=user)
        serializer.save(user=user, user_details=user_details)


class QuestionViewset(viewsets.ModelViewSet):
    serializer_class = QuestionSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (PostMethodAuthRequired,)
    http_method_names = ('get', 'post')
    filter_backends = (filters.SearchFilter,
                       filters.OrderingFilter, DjangoFilterBackend,)
    filter_fields = ['category', 'category__name',
                     'user', 'user_details__gender', 'content_type']
    ordering_fields = ('created_at', 'updated_at', 'download_count',)
    search_fields = ("title", 'content', 'slug')
    pagination_class = StandardResultsSetPagination

    # Cache page for 15 Minutes
    @method_decorator(cache_page(900))
    @method_decorator(vary_on_cookie)
    def dispatch(self, *args, **kwargs):
        return super(QuestionViewset, self).dispatch(*args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        qs = Question.objects.all()

        return self.serializer_class().setup_eager_loading(qs)

    def perform_create(self, serializer: QuestionSerializer):
        user = self.request.user
        user_details = UserDetail.objects.get(user=user)
        serializer.save(user=user, user_details=user_details)


class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    http_method_names = ('get',)
    filter_backends = (filters.SearchFilter, DjangoFilterBackend,)
    search_fields = ("username",)
    filter_fields = ('username',)
    pagination_class = StandardResultsSetPagination

    # Cache page for 1 day
    @method_decorator(cache_page(86400))
    @method_decorator(vary_on_cookie)
    def dispatch(self, *args, **kwargs):
        return super(UserViewset, self).dispatch(*args, **kwargs)


class CommentViewset(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    http_method_names = ('get', 'post',)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (PostMethodAuthRequired,)
    filter_backends = (filters.SearchFilter,
                       filters.OrderingFilter, DjangoFilterBackend,)
    ordering_fields = ('created_at', 'question', 'post',)
    search_fields = ("user__username", "question__slug", "post__slug",)
    filter_fields = ['question', 'post',
                     'parent', 'user_details__gender', 'user']
    pagination_class = StandardResultsSetPagination

    # # Cache page for 15 Minutes
    # @method_decorator(cache_page(900))
    # @method_decorator(vary_on_cookie)
    # def dispatch(self, *args, **kwargs):
    #     super(CommentViewset, self).dispatch(*args, **kwargs)

    # @ratelimit(key="ip", rate="3/m", block=False)

    def perform_create(self, serializer):
        user = self.request.user
        user_details = UserDetail.objects.get(user=user)
        user_gender = user_details.gender
        data = serializer.validated_data
        entity: Union[Question, Post] = data.get(
            'question') or data.get('post')
        hidden_author = data["hidden_author"] or False

        # Checking if users gender or is anonymous to comment on entity.
        if entity.disable_man and user_gender == GENDER_MALE:
            raise NotAcceptable({
                "status": False,
                "error": {
                    "code": "comment_disabled_for_your_gender",
                    "message": comment_disabled_for_your_gender
                }
            })

        # Checking if users gender or is anonymous to comment on entity.

        if entity.disable_woman and user_gender == GENDER_FEMALE:
            raise NotAcceptable({
                "status": False,
                "error": {
                    "code": "comment_disabled_for_your_gender",
                    "message": comment_disabled_for_your_gender
                }
            })

        # Checking if users gender or is anonymous to comment on entity.
        if entity.disable_anonymous and hidden_author:
            raise NotAcceptable({
                "status": False,
                "error": {
                    "code": "comment_disabled_for_your_anonymous",
                    "message": comment_disabled_for_your_anonymous
                }
            })

        serializer.save(user=user, user_details=user_details)


class CategoryViewset(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    http_method_names = ('get',)
    filter_fields = ['slug']
    filter_backends = [DjangoFilterBackend]

    # Cache page for 1 day
    @method_decorator(cache_page(86400))
    @method_decorator(vary_on_cookie)
    def dispatch(self, *args, **kwargs):
        return super(CategoryViewset, self).dispatch(*args, **kwargs)


class UserDetailViewset(viewsets.ModelViewSet):
    serializer_class = UserDetailSerializer
    http_method_names = ('get', 'put',)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('user__username',)
    pagination_class = StandardResultsSetPagination
    authentication_classes = (TokenAuthentication,)
    permission_classes = (PostMethodAuthRequired,)

    def get_queryset(self):
        queryset = UserDetail.objects.all()

        qs = self.serializer_class().setup_eager_loading(queryset)
        return qs

    def get_serializer_context(self):
        context = super().get_serializer_context()
        user__username = self.request.query_params.get('user__username')

        if user__username:
            context['additional_fields'] = True
        return context

    # Cache page for 1 hour

    @method_decorator(cache_page(3600))
    @method_decorator(vary_on_cookie)
    def dispatch(self, *args, **kwargs):
        return super(UserDetailViewset, self).dispatch(*args, **kwargs)

    # TODO: add hidden_profile feature


class PostAndQuestionViewset(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    http_method_names = ('get',)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ['slug']
    pagination_class = StandardResultsSetPagination

    # Cache page for 1 hour

    @method_decorator(cache_page(3600))
    @method_decorator(vary_on_cookie)
    def list(self, request, *args, **kwargs):
        posts = Post.objects
        questions = Question.objects
        slug = self.request.query_params.get('slug', None)

        if slug is not None and len(slug) > 0:
            posts = posts.filter(slug=slug, approved=True).select_related(
                "user", "category", "user_details")
            questions = questions.filter(slug=slug).select_related(
                "user", "category", "user_details")

        else:
            posts = posts.filter(approved=True).select_related(
                "user", "category", "user_details")
            questions = questions.all().select_related("user", "category", "user_details")

        post_serializer = PostSerializer(
            posts, many=True, context={'request': request})
        question_serializer = QuestionSerializer(
            questions, many=True, context={'request': request})
        page = self.paginate_queryset(posts)

        data = {}

        data['posts'] = post_serializer.data
        data['questions'] = question_serializer.data

        return self.get_paginated_response(data)


class NotificationViewset(viewsets.ModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    http_method_names = ['get', 'post', 'put']
    pagination_class = StandardResultsSetPagination

    def perform_create(self, serializer):
        executer_id = self.request.data.get('executer')
        if int(self.request.user.id) != int(executer_id):
            return False

        serializer.save()

    def perform_update(self, serializer):
        actor_id = self.request.data.get('actor')

        if int(self.request.user.id) != int(actor_id):
            return False

        serializer.save()

    def list(self, request, *args, **kwargs):
        posts = Notification.objects.filter(
            actor=request.user, target_type="post", readed=False)
        questions = Notification.objects.filter(
            actor=request.user, target_type="question", readed=False)
        comments = Notification.objects.filter(
            actor=request.user, target_type="comment", readed=False)

        data = []
        a = -1

        if len(posts) > 0:
            for i in posts:
                a += 1
                if int(i.executer.id) != int(request.user.id):
                    target = Notification.objects.get_post(i.target_object_id)
                    data.append({
                        'target': (PostSerializer(target['target'], many=True).data),
                        'details': NotificationSerializer(posts, many=True).data,
                        'executer_details':
                            UserDetailSerializer(UserDetail.objects.filter(user=i.executer.id), many=True,
                                                 context={'request': request}).data[0],
                        'actor_details': UserDetailSerializer(UserDetail.objects.filter(user=i.actor.id), many=True,
                                                              context={'request': request}).data[0],
                        'message': str(i),
                        'id': int(i.id),
                        'actor': str(i.actor.id),
                        'executer': str(i.executer.id)

                    })
        if len(questions) > 0:
            for i in questions:
                a += 1
                if int(i.executer.id) != int(request.user.id):
                    target = Notification.objects.get_question(
                        i.target_object_id)
                    data.append({
                        'target': (QuestionSerializer(target['target'], many=True).data),
                        'executer_details':
                            UserDetailSerializer(UserDetail.objects.filter(user=i.executer.id), many=True,
                                                 context={'request': request}).data[0],
                        'actor_details': UserDetailSerializer(UserDetail.objects.filter(user=i.actor.id), many=True,
                                                              context={'request': request}).data[0],
                        'message': str(i),
                        'id': int(i.id),
                        'actor': str(i.actor.id),
                        'executer': str(i.executer.id)
                    })
        if len(comments) > 0:
            for i in comments:
                a += 1
                if int(i.executer.id) != int(request.user.id):
                    target = Notification.objects.get_comment(
                        i.target_object_id)
                    data.append({
                        'target': (CommentSerializer(target['target'], many=True).data),
                        'executer_details':
                            UserDetailSerializer(UserDetail.objects.filter(user=i.executer.id), many=True,
                                                 context={'request': request}).data[0],
                        'actor_details': UserDetailSerializer(UserDetail.objects.filter(user=i.actor.id), many=True,
                                                              context={'request': request}).data[0],
                        'message': str(i),
                        'id': int(i.id),
                        'actor': str(i.actor.id),
                        'executer': str(i.executer.id)
                    })
        self.paginate_queryset(posts)

        return self.get_paginated_response(data)


class ImageUploader(viewsets.ViewSet):
    http_method_names = ['post']

    def create(self, request):
        from django.core.files.storage import FileSystemStorage
        from hashlib import md5
        from .helpers import compress_icon, is_valid_image
        from django.conf import settings
        from os import path
        from django.forms.utils import ValidationError

        allowed_extensions = [
            'png',
            'jpg',
            'jpeg',
            'gif'
        ]

        if request.FILES['image']:

            fs = FileSystemStorage(base_url=f'{settings.MEDIA_URL}/all',
                                   location=path.join(settings.MEDIA_ROOT, 'all'))
            image = request.FILES['image']
            image_name = str(image.name)
            image_extension = str(image_name.split('.')[1])
            checksum = f"{md5(image_name.encode('utf8')).hexdigest()}.{image_extension}"

            try:
                if is_valid_image(image):
                    compressed_image = compress_icon(image)
                    filename = fs.save(checksum, compressed_image)
                    uploaded_file_url = fs.url(filename)

                    return Response({
                        'status': True,
                        'url': f"{settings.URL}{uploaded_file_url}"
                    })

            except ValidationError as e:
                return Response({
                    'status': False,
                    'message': 'Vi stöder inte dessa filtillägg.'
                })

        else:
            return Response({
                'status': False
            })
