def excerpt(string, width: int):
    if len(string) > width:
        string = string[:width - 3] + '...'
    return string


def clean_html(html: str):
    from re import compile, sub
    cleanr = compile('<.*?>')
    cleantext = sub(cleanr, '', html)
    return cleantext


def is_valid_image(image):
    from PIL import Image
    from django.forms.utils import ValidationError

    try:
        im = Image.open(image)
        im.verify()

        return True
    except Exception as e:
        raise ValidationError(e)


def compress_icon(image):
    from PIL import Image
    from io import BytesIO
    from django.core.files.uploadedfile import InMemoryUploadedFile
    from sys import getsizeof

    im = Image.open(image)
    image_name = str(image)

    file_format = im.format
    output = BytesIO()

    if file_format == 'png':
        convert = im.convert(mode='P', palette=Image.ADAPTIVE)
    else:
        convert = im

    print(convert)

    width, height = im.size
    re = convert.resize((650, 380), Image.ANTIALIAS)

    re.save(output, format=file_format, quality=80, optimize=True)
    output.seek(0)

    return InMemoryUploadedFile(output, 'ImageField', f"{image_name.split('.')[0]}.{image_name.split('.')[1]}",
                                file_format, getsizeof(output), None)
