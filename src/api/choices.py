from django.apps import apps

CONTENT_TYPE_CHOCIES = (
    (0, 'Question'),
    (1, 'Ankät'),
    (2, 'Nyheter'),
)

app = apps.get_app_config('api')
NOTIFICATION_TYPE_CHOICES = []
for model_name, model in app.models.items():
    NOTIFICATION_TYPE_CHOICES.append((model_name, model_name))
