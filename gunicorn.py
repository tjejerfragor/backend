import multiprocessing, os

def when_ready(server):
    # touch app-initialized when ready
    open('/tmp/app-initialized', 'w').close()
bind = 'unix:///tmp/nginx.socket'
# PORT = os.environ.get("PORT") or 8000
# bind = '0.0.0.0:'+PORT
workers = multiprocessing.cpu_count() * 2 + 1
gzip = True
gzip_vary = True
gzip_proxied = True
gzip_min_length = "1k"
gzip_buffers = "16 8k"
gzip_http_version = 1.1
gzip_comp_level = 9
gzip_types = """text/plain
                  text/javascript
                  text/css
                  text/xml
                  application/json
                  application/javascript
                  application/atom+xml
                  application/rss+xml
                  application/x-javascript
                  application/xml
                  application/xhtml+xml
                  application/x-font-ttf
image/svg+xml"""